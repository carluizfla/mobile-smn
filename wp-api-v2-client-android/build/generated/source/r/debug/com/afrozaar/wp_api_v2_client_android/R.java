/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.afrozaar.wp_api_v2_client_android;

public final class R {
    public static final class dimen {
        public static int media_thumb_size = 0x7f080001;
    }
    public static final class integer {
        public static int wp_parent_category = 0x7f0d0001;
    }
    public static final class string {
        public static int app_name = 0x7f150001;
        public static int content_audio_uri = 0x7f150002;
        public static int content_image_uri = 0x7f150003;
        public static int content_location = 0x7f150004;
        public static int content_video_uri = 0x7f150005;
        public static int post_type = 0x7f150006;
        public static int pref_id_acc_type = 0x7f150007;
        public static int pref_id_email = 0x7f150008;
        public static int pref_id_first_name = 0x7f150009;
        public static int pref_id_has_profile = 0x7f15000a;
        public static int pref_id_last_name = 0x7f15000b;
        public static int pref_id_name = 0x7f15000c;
        public static int pref_id_password = 0x7f15000d;
        public static int pref_id_profile_pic = 0x7f15000e;
        public static int pref_id_username = 0x7f15000f;
        public static int pref_id_wordpress_id = 0x7f150010;
        public static int pref_id_wordpress_password = 0x7f150011;
        public static int pref_id_wordpress_username = 0x7f150012;
        public static int pref_id_wp_login_pass = 0x7f150013;
        public static int pref_id_wp_login_user = 0x7f150014;
        public static int pref_id_wp_server_ip = 0x7f150015;
        public static int pref_id_wp_server_port = 0x7f150016;
        public static int pref_title_wp = 0x7f150017;
        public static int user_role = 0x7f150018;
        public static int wp_create_post_header1 = 0x7f150019;
        public static int wp_create_post_header2 = 0x7f15001a;
        public static int wp_media_delete = 0x7f15001b;
        public static int wp_media_edit = 0x7f15001c;
        public static int wp_post_publish = 0x7f15001d;
        public static int wp_text_address = 0x7f15001e;
        public static int wp_text_categories = 0x7f15001f;
        public static int wp_text_headline = 0x7f150020;
        public static int wp_text_location = 0x7f150021;
        public static int wp_text_photos = 0x7f150022;
        public static int wp_text_summary = 0x7f150023;
    }
}
