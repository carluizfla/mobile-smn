package com.example.fresh.modulio.attachmentviewer.loader;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;

import com.example.fresh.modulio.R;
import com.example.fresh.modulio.attachmentviewer.model.MediaAttachment;
import com.example.fresh.modulio.attachmentviewer.ui.AttachmentFragment;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class PicassoImageLoader  extends MediaLoader {

    public PicassoImageLoader(MediaAttachment attachment) {
        super(attachment);
    }

    @Override
    public boolean isImage() {
        return true;
    }

    @Override
    public void loadMedia(AttachmentFragment context, final ImageView imageView, View rootView, final MediaLoader.SuccessCallback callback) {
        Picasso.with(context.getContext())
                .load(((MediaAttachment) getAttachment()).getUrl())
                .placeholder(R.drawable.placeholder_image)
                .into(imageView, new ImageCallback(callback));
    }

    @Override
    public void loadThumbnail(Context context, ImageView thumbnailView, MediaLoader.SuccessCallback callback) {
        Picasso.with(context)
                .load(((MediaAttachment) getAttachment()).getThumbnailUrl())
                .resize(100, 100)
                .placeholder(R.drawable.placeholder_image)
                .centerInside()
                .into(thumbnailView, new ImageCallback(callback));
    }

    private static class ImageCallback implements Callback {

        private final MediaLoader.SuccessCallback callback;

        public ImageCallback(SuccessCallback callback) {
            this.callback = callback;
        }

        @Override
        public void onSuccess() {
            callback.onSuccess();
        }

        @Override
        public void onError() {

        }
    }

}

