package com.example.fresh.modulio.ui;

import android.animation.ValueAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.rest.HttpServerErrorResponse;
import com.afrozaar.wp_api_v2_client_android.rest.WordPressRestResponse;
import com.example.fresh.modulio.adapter.TabAdapter;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.widgets.CustomAppBarLayout;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.WpClient;
import com.example.fresh.modulio.adapter.MapsAdapter;
import com.example.fresh.modulio.widgets.MapViewPager;
import com.google.android.gms.maps.SupportMapFragment;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class MapsFragment extends Fragment implements MapViewPager.Callback, CustomAppBarLayout.OnStateChangeListener {

    //Layout
    private ViewPager viewPager;
    private MapViewPager mvp;
    private FrameLayout fr;
    private SupportMapFragment map;
    private LinearLayout loadingView;

    //List
    private MapsAdapter adapter;
    private List<Post> locations;

    //Paging
    private int PAGE_SIZE = 10;
    private int PAGE;
    private boolean canLoadMore = true;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        fr = (FrameLayout) inflater.inflate(R.layout.fragment_maps,null);

        setHasOptionsMenu(true);

        map = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        viewPager = (ViewPager) fr.findViewById(R.id.viewPager);
        viewPager.setPageMargin(Math.round(getResources().getDimension(R.dimen.spacing_large)));
        loadingView = (LinearLayout) fr.findViewById(R.id.loading_view) ;

        onLastPageReached();

        return fr;
    }

    @Override
    public void onResume(){
        super.onResume();

        setUserVisibleHint(true);
    }

    @Override
    public void onPause() {
        super.onPause();

        setUserVisibleHint(false);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        if (!(getActivity() instanceof MainActivity)) return;
        CustomAppBarLayout appBarLayout = (CustomAppBarLayout) (getActivity()).findViewById(R.id.app_bar_layout);

        // Uncomment this to always show toolbar with maps layout
        // appBarLayout.setExpanded(true);

        //(Un)Register listener for toolbar visibility so we can adjust bottom margin
        if (!isVisibleToUser){
            appBarLayout.removeOnStateChangeListener(this);
        } else {
            appBarLayout.addOnStateChangeListener(this);
        }

        //When we become visible, also update
        if (isVisibleToUser) {
                boolean hidden = appBarLayout.isToolbarHidden();
                updateBottomMargins(hidden);
        }
    }

    private void updateBottomMargins(boolean toolbarHidden){
        //Adjust the bottom margin depending on if the actionbar is showing
        if (!toolbarHidden) {

            TypedValue tv = new TypedValue();
            if (getActivity() != null && getActivity().getTheme().resolveAttribute(android.R.attr.actionBarSize, tv, true)) {
                animateBottomMarginTo(TypedValue.complexToDimensionPixelSize(tv.data,getResources().getDisplayMetrics()));
            }

        } else {
            animateBottomMarginTo(0);
        }
    }

    private void animateBottomMarginTo(final int bottomMargin){
        final ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) viewPager.getLayoutParams();

        ValueAnimator animator = ValueAnimator.ofInt(lp.bottomMargin, bottomMargin);
        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator)
            {
                lp.bottomMargin = (Integer) valueAnimator.getAnimatedValue();
                viewPager.requestLayout();
            }
        });
        animator.setDuration(100);
        animator.start();
    }

    @Override
    public void onMapViewPagerReady() {
        mvp.getMap().setPadding(
                0,
                Math.round(getResources().getDimension(R.dimen.spacing_large)),
                0,
                viewPager.getHeight());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = (fm.findFragmentById(R.id.map));
        FragmentTransaction ft = fm.beginTransaction();
        ft.remove(fragment);
        ft.commitAllowingStateLoss();
    }

    @Override
    public void onStateChange(CustomAppBarLayout.State toolbarChange) {
        updateBottomMargins(toolbarChange == CustomAppBarLayout.State.COLLAPSED);
    }


    @Override
    public void onLastPageReached() {
        if (!canLoadMore) return;

        //Load the next page
        PAGE += 1;

        HashMap<String,String> params = CommonUtils.addFilters(new HashMap<String, String>(),
                getArguments().getString(TabAdapter.EXTRA_FILTERON),
                getArguments().getString(TabAdapter.EXTRA_FILTER));


        //Create a new client with the updated credientials and use it to get more info..
        WpClient.init(getActivity()).getPostsForPage(new WordPressRestResponse<List<Post>>() {
            @Override
            public void onSuccess(List<Post> posts) {
                //Check if we can load more items
                canLoadMore = (posts.size() == PAGE_SIZE);

                if (locations == null)  //i.e. first load
                    locations = new ArrayList<Post>();

                //Add all the valid locations to the list and notify the adapter
                for (Post post : posts){
                    if (!post.getLocation().isEmpty()){
                        locations.add(post);
                    }
                }

                //If we have no posts to show
                if (locations.size() == 0) {
                    showEmptyView();
                } else if (adapter == null) { //i.e. first load init adapter, else update it
                    adapter = new MapsAdapter(getChildFragmentManager(), locations);

                    if (getActivity() != null)
                    mvp = new MapViewPager.Builder(getActivity())
                            .mapFragment(map)
                            .viewPager(viewPager)
                            .position(0)
                            .adapter(adapter)
                            .callback(MapsFragment.this)
                            .build();

                    if (loadingView.getVisibility() == View.VISIBLE){
                        loadingView.setVisibility(View.GONE);
                    }
                } else {
                    adapter.updateAdapter(locations, mvp);
                }
            }

            @Override
            public void onFailure(HttpServerErrorResponse errorResponse) {
                showEmptyView();
            }
        }, PAGE, params);
    }

    public void showEmptyView(){
        if (loadingView.getVisibility() == View.VISIBLE)
            loadingView.setVisibility(View.GONE);

        fr.findViewById(R.id.empty_view).setVisibility(View.VISIBLE);
    }


}
