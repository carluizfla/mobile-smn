package com.example.fresh.modulio.attachmentviewer.model;

import com.afrozaar.wp_api_v2_client_android.model.Media;

import java.io.Serializable;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public abstract class Attachment implements Serializable {

    public abstract String getDescription();
}
