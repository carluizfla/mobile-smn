package com.example.fresh.modulio.attachmentviewer.model;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class LinkAttachment extends Attachment {

    private String url;
    private String title;

    public LinkAttachment(String url, String title){
        this.url = url;
        this.title = title;
    }

    public String getUrl() { return url; }

    @Override
    public String getDescription() {
        return title;
    }

}
