package com.example.fresh.modulio;

import android.view.Menu;
import android.view.MenuItem;

import com.example.fresh.modulio.adapter.ItemAdapter;
import com.example.fresh.modulio.menu.Action;
import com.example.fresh.modulio.menu.SimpleMenu;
import com.example.fresh.modulio.menu.SimpleSubMenu;
import com.example.fresh.modulio.ui.MapsFragment;
import com.example.fresh.modulio.ui.PrimaryFragment;

import java.lang.ref.PhantomReference;
import java.util.ArrayList;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class Config {

    //The URL to the backend API base (not ending with a slash).
    public static String BACKEND_URL = "http://backend.soumaisniteroi.com.br";
    //Config json location to load app's menu from. Leave empty to use local config.json in assets
    public static String CONFIG_URL = ""; 

    //Disables the featured first entry
    public static boolean NEVER_FEATURE_CARD = true;
    //Ensures that that featured image are never included in attachments at all times
    public static boolean FORCE_FEATURED_IMAGE_NEVER_ATTACHMENT = false;
    //Force the layout of rows in the list. Set to AUTO for default behavior.
    public static int FORCE_ROW_STYLE = ItemAdapter.AUTO;
    //Always show primary attachment in viewer directly upon item selection (if applicable).
    public static boolean SKIP_DETAILVIEW_IF_ATTACHMENT = false;
    //Uses HardcodedConfig.java instead of JSON to get configuration. Might be faster in some cases, and you can apply logic.
    public static boolean USE_HARDCODED_CONFIG = false;

    //Where users can manage their account. Leave empty to hide link from settings
    public static String USER_LOGIN_URL = BACKEND_URL + "/wp-login.php";
    //The API baseurl
    public static String API_BASE = BACKEND_URL + "/wp-json/wp/v2/";

    /**
     * {
     "title": "Demo",
     "drawable": "ic_menu_gallery",
     "submenu": "",
     "tabs": [
     {
     "title": "All",
     "type": "List"
     },
     {
     "title": "By Tag",
     "type": "List",
     "filterOn": "Tag",
     "filter": "3"
     },
     {
     "title": "By Category",
     "type": "List",
     "filterOn": "Category",
     "filter": "5"
     }
     ]
     },
     */
}
