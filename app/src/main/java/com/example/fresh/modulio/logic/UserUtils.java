package com.example.fresh.modulio.logic;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.afrozaar.wp_api_v2_client_android.model.User;
import com.afrozaar.wp_api_v2_client_android.rest.HttpServerErrorResponse;
import com.afrozaar.wp_api_v2_client_android.rest.WordPressRestResponse;
import com.afrozaar.wp_api_v2_client_android.util.LoginAccountHelper;
import com.afrozaar.wp_api_v2_client_android.util.WordpressPreferenceHelper;
import com.example.fresh.modulio.WpClient;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class UserUtils {

    private Context context;

    public UserUtils(Context context){
        this.context = context;
    }

    /**
     * After a login/registration use this method to save the userinfo.
     * This info is to be used for display in the UI, as well as auth for making API requests
     * @param user A User object, as returned by getUser() or getMe()
     * @param username Username of the user
     * @param password Password of the user
     */
    public void saveAndCompleteUser(User user, String username, String password, final Callback callback){
        //Save the connection/auth details
        WordpressPreferenceHelper.with(context)
                .setWordPressUsername(username)
                .setWordPressUserPassword(password)
                .setWordPressUserId(user.getId());

        //Save the additional details
        LoginAccountHelper.with(context)
                .setUserId(Long.toString(user.getId()));

        //Create a new client with the updated credientials and use it to get more info..
        WpClient.init(context).getUserFromLogin(username, new WordPressRestResponse<User>() {
            @Override
            public void onSuccess(User result) {
                LoginAccountHelper.with(context).setUserEmail(result.getEmail()).setUserName(result.getName());
                callback.userInfoCompleted();
            }

            @Override
            public void onFailure(HttpServerErrorResponse errorResponse) {
                callback.userInfoCompleted();
            }
        });

        String firstAvatarUrl = (user.getAvatarUrls().containsKey("96")) ? user.getAvatarUrls().get("96") : null;
        if (firstAvatarUrl != null)
            LoginAccountHelper.with(context).setUserProfilePic(firstAvatarUrl);
    }

    public void resetUserInfo(){
        //Actually logging out also requires calling 'WpClient.init()' afterwards to start a client with updated credentials

        WordpressPreferenceHelper.with(context).resetUserState();
        LoginAccountHelper.with(context).resetUserState();
    }

    public boolean isUserLoggedIn(){
        WordpressPreferenceHelper helper = WordpressPreferenceHelper.with(context);
        return ! TextUtils.isEmpty(helper.getWordPressUserPassword()) && ! TextUtils.isEmpty(helper.getWordPressUsername());
    }

    public interface Callback{
        void userInfoCompleted();
    }
}
