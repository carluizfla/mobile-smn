package com.example.fresh.modulio;

import android.content.Context;

import com.afrozaar.wp_api_v2_client_android.rest.WpClientRetrofit;
import com.afrozaar.wp_api_v2_client_android.util.WordpressPreferenceHelper;
import com.example.fresh.modulio.logic.UserUtils;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class WpClient {
    private static WpClientRetrofit wpClientRetrofit;
    private static boolean isAnonymous;

    /**
     * Get the WpClient, will be created if needed.
     * @param context Context to create the client with
     * @return wpClientRetrofit to retrieve content
     */
    public static WpClientRetrofit get(Context context){
        if (wpClientRetrofit == null){
            wpClientRetrofit = newClient(context);
        }

        return wpClientRetrofit;
    }

    /**
     * Init a new WpClient with the latest user details (e.g. use this when user has just logged in or out)
     * @param context Context to create the client with
     * @return wpClientRetrofit to retrieve content
     */
    public static WpClientRetrofit init(Context context){
        wpClientRetrofit = newClient(context);

        return wpClientRetrofit;
    }

    public static boolean isIsAnonymous(){
        return isAnonymous;
    }

    private static WpClientRetrofit newClient(Context context){
        if (!new UserUtils(context).isUserLoggedIn()){
            //Init a new anonymous client
            isAnonymous = true;
            return new WpClientRetrofit(Config.API_BASE, "","");
        } else {
            //Init a new client with username and password
            isAnonymous = false;
            WordpressPreferenceHelper helper = WordpressPreferenceHelper.with(context);
            return new WpClientRetrofit(Config.API_BASE, helper.getWordPressUsername(), helper.getWordPressUserPassword());
        }
    }
}
