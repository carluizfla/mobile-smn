package com.example.fresh.modulio.menu;

import android.view.MenuItem;

import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public interface MenuItemCallback {

    void menuItemClicked(List<Action> action, MenuItem item);
}
