package com.example.fresh.modulio.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.example.fresh.modulio.menu.Action;
import com.example.fresh.modulio.ui.MapsFragment;
import com.example.fresh.modulio.ui.OverviewFragment;
import com.example.fresh.modulio.ui.PrimaryFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class TabAdapter  extends FragmentStatePagerAdapter {

    public static String EXTRA_FILTERON = "filteron";
    public static String EXTRA_FILTER = "filter";

    List<Action> actions;

    public TabAdapter(FragmentManager fm, List<Action> action) {
        super(fm);
        this.actions = action;
    }

    /**
     * Return fragment with respect to Position .
     */
    @Override
    public Fragment getItem(int position)
    {
        return fragmentFromAction(actions.get(position));
    }

    @Override
    public int getCount() {
        return actions.size();
    }

    /**
     * This method returns the title of the tab according to the position.
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return actions.get(position).name;
    }

    public static Fragment fragmentFromAction(Action action){
        Fragment fragment;

        if (action.mClass.equals(MapsFragment.class))
            fragment = new MapsFragment();
        else if (action.mClass.equals(OverviewFragment.class))
            fragment = new OverviewFragment();
        else
            fragment = new PrimaryFragment();

        Bundle args = new Bundle();
        args.putString(EXTRA_FILTERON, action.filterOn);
        args.putString(EXTRA_FILTER, action.filter);

        fragment.setArguments(args);

        return fragment;
    }
}
