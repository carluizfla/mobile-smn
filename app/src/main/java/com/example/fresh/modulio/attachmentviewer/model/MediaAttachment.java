package com.example.fresh.modulio.attachmentviewer.model;

import com.afrozaar.wp_api_v2_client_android.model.Media;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class MediaAttachment extends Attachment {

    private String url;
    private String thumburl;
    private String description;
    private String mime;

    //Mimes
    public static String MIME_IMAGE = "image";
    public static String MIME_FILE = "file";
    public static String MIME_PATTERN_VID = "video/";
    public static String MIME_PATTERN_AUDIO = "audio/";

    public MediaAttachment(Media media, String mime){
        if (media.getAltText() != null && !media.getAltText().isEmpty()){
            description = media.getAltText();
        } else if  (media.getCaption() != null && !media.getCaption().isEmpty()){
            description = media.getCaption();
        } else if  (media.getDescription() != null && !media.getDescription().isEmpty()){
            description = media.getDescription();
        } else if  (media.getTitle() != null && !media.getTitle().getRendered().isEmpty()) {
            description = media.getTitle().getRendered();
        }

        this.mime = mime;
        this.url = media.getSourceUrl();
        this.thumburl = media.getGoodSourceUrl();
    }

    public MediaAttachment(String url, String mime, String title){
        this.mime = mime;
        this.url = url;
        this.description = title;
        this.thumburl = url;
    }

    public String getUrl() {
        return url;
    }

    public String getThumbnailUrl() {
        return thumburl;
    }

    public String getMime() {
        return mime;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description){
        this.description = description;
    }

}
