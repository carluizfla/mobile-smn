package com.example.fresh.modulio.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.net.UrlQuerySanitizer;
import android.os.Parcelable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.rest.HttpServerErrorResponse;
import com.afrozaar.wp_api_v2_client_android.rest.WordPressRestResponse;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.WpClient;
import com.example.fresh.modulio.logic.ChromeHelper;

import java.util.ArrayList;
import java.util.List;

public class DeeplinkActivity extends AppCompatActivity {

    public static String EXTRA_PID = "post_id";

    public static void showActivityForPost(String pid, Context context){
        Intent intent = new Intent(context, DeeplinkActivity.class);
        intent.putExtra(EXTRA_PID, pid);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deeplink);

        Intent intent = getIntent();
        Uri data = intent.getData();

        String value = null;

        if (data != null) {
            UrlQuerySanitizer sanitizer = new UrlQuerySanitizer(data.toString());
            value = sanitizer.getValue("p");
        }

        if (value == null && getIntent().hasExtra(EXTRA_PID)){
            value = getIntent().getExtras().getString(EXTRA_PID);
        }

        if (value == null){
            //Open in browser (and not in own app again)
            Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.toString()));
            PackageManager packageManager = getPackageManager();
            List<ResolveInfo> activities = packageManager.queryIntentActivities(myIntent, 0);
            ArrayList<Intent> targetIntents = new ArrayList<>();
            for (ResolveInfo currentInfo : activities) {
                String packageName = currentInfo.activityInfo.packageName;
                if (!getApplicationContext().getPackageName().equals(packageName)) {
                    Intent targetIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(data.toString()));
                    targetIntent.setPackage(packageName);
                    targetIntents.add(targetIntent);
                }
            }
            if(targetIntents.size() > 0) {
                Intent chooserIntent = Intent.createChooser(targetIntents.remove(0), getResources().getString(R.string.open_with));
                chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, targetIntents.toArray(new Parcelable[] {}));
                startActivity(chooserIntent);
            }
            finish();
        } else {
            WpClient.init(this).getPost(Long.parseLong(value), new WordPressRestResponse<Post>() {
                @Override
                public void onSuccess(Post result) {
                    DetailActivity.navigate(DeeplinkActivity.this, null, null, result);
                    finish();
                }

                @Override
                public void onFailure(HttpServerErrorResponse errorResponse) {
                    Toast.makeText(DeeplinkActivity.this, getString(R.string.loading_failed), Toast.LENGTH_LONG).show();
                    finish();
                }
            });
        }
    }
}
