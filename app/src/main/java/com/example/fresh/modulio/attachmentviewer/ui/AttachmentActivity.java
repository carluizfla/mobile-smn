package com.example.fresh.modulio.attachmentviewer.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.attachmentviewer.model.Attachment;
import com.example.fresh.modulio.attachmentviewer.model.LocationAttachment;
import com.example.fresh.modulio.attachmentviewer.model.MediaAttachment;
import com.example.fresh.modulio.attachmentviewer.loader.GoogleMapsLoader;
import com.example.fresh.modulio.attachmentviewer.loader.MediaLoader;
import com.example.fresh.modulio.attachmentviewer.widgets.ScrollGalleryView;
import com.example.fresh.modulio.attachmentviewer.loader.DefaultAudioLoader;
import com.example.fresh.modulio.attachmentviewer.loader.DefaultVideoLoader;
import com.example.fresh.modulio.attachmentviewer.loader.PicassoImageLoader;

import java.util.ArrayList;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class AttachmentActivity extends AppCompatActivity {

    private ScrollGalleryView scrollGalleryView;
    private List<MediaLoader> mediaList;

    public static String IMAGES = "images";

    public static void startActivity(Activity source, ArrayList<Attachment> images){
        Intent intent = new Intent(source, AttachmentActivity.class);
        intent.putExtra(IMAGES, images);
        source.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attachment);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        ArrayList<Attachment> images = (ArrayList<Attachment>) getIntent().getSerializableExtra(IMAGES);
        scrollGalleryView = (ScrollGalleryView) findViewById(R.id.scroll_gallery_view);

        CommonUtils.loadAdmob(findViewById(R.id.adView));

        mediaList = easyInitView(scrollGalleryView, images, getSupportFragmentManager());
    }

    public static List<MediaLoader> easyInitView(ScrollGalleryView view, ArrayList<Attachment> images, FragmentManager fm){

        List<MediaLoader> infos = new ArrayList<>(images.size());
        for (Attachment attachment : images) {
            if (attachment instanceof MediaAttachment) {
                MediaAttachment mediaAttachment = ((MediaAttachment) attachment);
                if (mediaAttachment.getMime().equals(MediaAttachment.MIME_IMAGE))
                    infos.add(new PicassoImageLoader(mediaAttachment));
                else if (mediaAttachment.getMime().contains(MediaAttachment.MIME_PATTERN_VID))
                    infos.add(new DefaultVideoLoader(mediaAttachment));
                else if (mediaAttachment.getMime().contains(MediaAttachment.MIME_PATTERN_AUDIO))
                    infos.add(new DefaultAudioLoader(mediaAttachment));
            } else if (attachment instanceof LocationAttachment){
                infos.add(new GoogleMapsLoader((LocationAttachment) attachment));
            }
        }

        view.setThumbnailSize((int) view.getContext().getResources().getDimension(R.dimen.thumbnail_height))
                .setZoom(true)
                .setFragmentManager(fm)
                .addMedia(infos);

        if (infos.size() == 1){
            view.hideThumbnails(true);
        }

        return infos;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
