package com.example.fresh.modulio.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.afrozaar.wp_api_v2_client_android.util.LoginAccountHelper;
import com.example.fresh.modulio.HardcodedConfig;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.Config;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.adapter.TabAdapter;
import com.example.fresh.modulio.logic.UserUtils;
import com.example.fresh.modulio.menu.Action;
import com.example.fresh.modulio.menu.MenuItemCallback;
import com.example.fresh.modulio.menu.SimpleMenu;
import com.example.fresh.modulio.logic.ConfigParser;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class MainActivity extends AppCompatActivity
        implements MenuItemCallback, ConfigParser.CallBack {

    private TabLayout tabLayout;
    private ViewPager viewPager;
    private NavigationView navigationView;
    private TabAdapter adapter;
    private static SimpleMenu menu;

    @Override
    public void configLoaded(boolean facedException) {
        if (facedException)
            Toast.makeText(this, getResources().getString(R.string.invalid_configuration), Toast.LENGTH_LONG).show();
        else
            //Load the first item
            menuItemClicked(menu.getFirstMenuItem().getValue(), menu.getFirstMenuItem().getKey());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_NoActionBar_TransparentStatus);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Check if there is a backend url
        if (Config.BACKEND_URL.isEmpty() || !Config.BACKEND_URL.contains("http")){
            Toast.makeText(this, "You need to provide a valid backend url", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        //Notifications
        if (getIntent().getExtras() != null && getIntent().getExtras().containsKey("pid")) {
            String pid = getIntent().getStringExtra("pid");
            DeeplinkActivity.showActivityForPost(pid, this);
        } //else; No post ID, just show mainActivity

        //Advertisements
        if (!getResources().getString(R.string.banner_app_id).equals(""))
            MobileAds.initialize(getApplicationContext(), getResources().getString(R.string.banner_app_id));
        CommonUtils.loadAdmob(findViewById(R.id.adView));

        //Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        //Layouts
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        viewPager = (ViewPager) findViewById(R.id.viewpager);

        //Menu items
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        menu = new SimpleMenu(navigationView.getMenu(), this);
        if (Config.USE_HARDCODED_CONFIG) {
            HardcodedConfig.configureMenu(menu, this);
        } else if (!Config.CONFIG_URL.isEmpty() && Config.CONFIG_URL.contains("http"))
            new ConfigParser(Config.CONFIG_URL, menu, this, this).execute();
        else
            new ConfigParser("config.json", menu, this, this).execute();

        showUserInfo();

        tabLayout.setupWithViewPager(viewPager);

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void menuItemClicked(List<Action> actions, MenuItem item) {
        //Uncheck all other items, check the current item
        for (MenuItem menuItem : menu.getMenuItems())
            menuItem.setChecked(false);
        item.setChecked(true);

        //Close the drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        //Load the new tabs
        adapter = new TabAdapter(getSupportFragmentManager(), actions);
        viewPager.setAdapter(adapter);

        //Show or hide the tab bar depending on if we need it
        if (actions.size() == 1)
            tabLayout.setVisibility(View.GONE);
        else
            tabLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
    }

    @Override
    public void onResume(){
        super.onResume();
        showUserInfo();
    }

    private void showUserInfo(){
        if (navigationView.getHeaderCount() == 0) return;

        View headerView = navigationView.getHeaderView(0);
        View loggedOutView = headerView.findViewById(R.id.loggedOutView);
        View loggedInView = headerView.findViewById(R.id.loggedInView);

        LoginAccountHelper login = LoginAccountHelper.with(this);

        if (new UserUtils(this).isUserLoggedIn()) {
            loggedOutView.setVisibility(View.GONE);
            loggedInView.setVisibility(View.VISIBLE);

            ((TextView) headerView.findViewById(R.id.emailView))
                    .setText(login.getUserEmail());
            ((TextView) headerView.findViewById(R.id.userView))
                    .setText(login.getUserName());

            Picasso.with(this).load(login.getUserProfilePic()).
                    error(R.drawable.ic_account_circle_white_48dp).
                    into((ImageView) headerView.findViewById(R.id.avatarView));
        } else {
            loggedInView.setVisibility(View.GONE);
            loggedOutView.setVisibility(View.VISIBLE);

            loggedOutView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            });
        }
    }
}
