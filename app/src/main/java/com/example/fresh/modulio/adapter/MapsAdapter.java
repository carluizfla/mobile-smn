package com.example.fresh.modulio.adapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Pair;

import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.util.DataConverters;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.ui.MapsItemFragment;
import com.example.fresh.modulio.widgets.MapViewPager;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class MapsAdapter extends MapViewPager.MultiAdapter {

    private List<Post> locations;

    public static int FINAL_ZOOM = 15;

    public MapsAdapter(FragmentManager fm, List<Post> locations)  {
        super(fm);
        this.locations = locations;
    }

    public void updateAdapter(List<Post> locations, MapViewPager pager){
        this.locations = locations;

        notifyDataSetChanged();
        if (pager.getMap() != null) //security for if the map hasn't loaded yet.
            pager.populate();
    }

    @Override
    public int getCount() {
        return locations.size();
    }

    @Override
    public Fragment getItem(int position) {
        return MapsItemFragment.newInstance(locations.get(position));
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return CommonUtils.stripHtml(locations.get(position).getTitle().getRendered());
    }

    @Override
    public String getMarkerTitle(int page, int position) {
        return CommonUtils.stripHtml(locations.get(page).getTitle().getRendered()).toString();
    }

    @Override
    public List<CameraPosition> getCameraPositions(int page) {
        List<CameraPosition> list = new ArrayList<CameraPosition>();

        Pair<Double, Double> location = DataConverters.parseLocation(locations.get(page).getLocation());
        LatLng latLng = new LatLng(location.first, location.second);
        CameraPosition position = CameraPosition.fromLatLngZoom(latLng, FINAL_ZOOM);

        list.add(position);

        return list;
    }

}
