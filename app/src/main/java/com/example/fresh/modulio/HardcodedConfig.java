package com.example.fresh.modulio;

import com.example.fresh.modulio.adapter.ItemAdapter;
import com.example.fresh.modulio.logic.ConfigParser;
import com.example.fresh.modulio.menu.Action;
import com.example.fresh.modulio.menu.SimpleMenu;
import com.example.fresh.modulio.menu.SimpleSubMenu;
import com.example.fresh.modulio.ui.MapsFragment;
import com.example.fresh.modulio.ui.PrimaryFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class HardcodedConfig {

    /**
     * Use of hardcoded config is at your own risk. We do provide some sample code below.
     * If you are not familiar with Java development, please use a JSON configuration instead as documented.
     * @param menu Add the menu items to this menu
     * @param callback Call this callback when you've finished adding items to the menu
     */
     public static void configureMenu(SimpleMenu menu, ConfigParser.CallBack callback){

         List<Action> firstTabs = new ArrayList<Action>();
         firstTabs.add(new Action.Builder("Tab 1").withFragment(PrimaryFragment.class).build());
         firstTabs.add(new Action.Builder("Tab 2").withFragment(MapsFragment.class).withFilter(ConfigParser.FILTER_TAG,"1").build());
         menu.add("Title", R.drawable.ic_menu_gallery, firstTabs);

         List<Action> secondTabs = new ArrayList<Action>();
         secondTabs.add(new Action.Builder("Tab 1").withFragment(MapsFragment.class).build());
         secondTabs.add(new Action.Builder("Tab 2").withFragment(PrimaryFragment.class).build());
         secondTabs.add(new Action.Builder("Tab 3").withFragment(MapsFragment.class).build());
         menu.add("Another", R.drawable.ic_menu_share, secondTabs);

         SimpleSubMenu sub = new SimpleSubMenu(menu, "Test");
         List<Action> thirdTabs = new ArrayList<Action>();
         thirdTabs.add(new Action.Builder("Tab 1").withFragment(PrimaryFragment.class).build());
         sub.add("Title 1", R.drawable.ic_menu_gallery, thirdTabs);

         callback.configLoaded(false);
     }
}
