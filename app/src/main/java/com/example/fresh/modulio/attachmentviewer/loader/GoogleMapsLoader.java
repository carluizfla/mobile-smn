package com.example.fresh.modulio.attachmentviewer.loader;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.example.fresh.modulio.R;
import com.example.fresh.modulio.attachmentviewer.model.LocationAttachment;
import com.example.fresh.modulio.attachmentviewer.model.SerializableLatLng;
import com.example.fresh.modulio.attachmentviewer.ui.AttachmentFragment;
import com.example.fresh.modulio.logic.CommonUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class GoogleMapsLoader extends MediaLoader implements OnMapReadyCallback {

    public static int FINAL_ZOOM = 15;

    private int bottomPadding = 0;
    private SerializableLatLng location;

    public GoogleMapsLoader(LocationAttachment attachment) {
        super(attachment);
    }

    @Override
    public boolean isImage() {
        return false;
    }

    @Override
    public void loadMedia(final AttachmentFragment context, ImageView imageView, View rootView, SuccessCallback callback) {
        imageView.setVisibility(View.GONE);
    }

    @Override
    public void loadThumbnail(Context context, ImageView thumbnailView, SuccessCallback callback) {
        thumbnailView.setImageBitmap(((BitmapDrawable) context.getResources().getDrawable(R.drawable.ic_location)).getBitmap());
        callback.onSuccess();
    }

    public void loadGoogleMaps(final Activity activity, View view, Bundle savedInstanceState){
        FrameLayout fragment = (FrameLayout) view.findViewById(R.id.fragment);

        LocationAttachment loc = (LocationAttachment) getAttachment();
        location = new SerializableLatLng(new LatLng(loc.getLat(), loc.getLon()));

        // Create MapView
        GoogleMapOptions options = new GoogleMapOptions();
        options.camera(new CameraPosition(location.get(), 10, 0, 0));
        MapView mapView = new MapView(activity, options);
        mapView.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT));
        mapView.onCreate(new Bundle());
        fragment.addView(mapView);

        // Gets to GoogleMap from the MapView and does initialization stuff
        mapView.getMapAsync(this);

        // Needs to call MapsInitializer before doing any CameraUpdateFactory calls
        MapsInitializer.initialize(activity);

        view.findViewById(R.id.bottomHolder).getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                bottomPadding = activity.findViewById(R.id.bottomHolder).getHeight();
            }
        });

        mapView.onResume();
    }

    @Override
    public void onMapReady(GoogleMap map) {
        map.getUiSettings().setMyLocationButtonEnabled(false);
        map.setPadding(0, 0, 0, bottomPadding);
        //map.setMyLocationEnabled(false);

        // Updates the location and zoom of the MapView
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(location.get(), FINAL_ZOOM);
        map.animateCamera(cameraUpdate);

        CharSequence markerTitle = CommonUtils.stripHtml(getAttachment().getDescription());
        Marker marker = map.addMarker(new MarkerOptions().position(location.get()).title(markerTitle.toString()));
        marker.showInfoWindow();
    }
}
