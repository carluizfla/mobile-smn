package com.example.fresh.modulio.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;

import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.logic.WebHelper;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class MapsItemFragment extends Fragment {

    private static String EXTRA_POST = "post";

    private Toolbar toolbar;
    private Post location;
    WebView contentView;

    public MapsItemFragment() { }

    public static MapsItemFragment newInstance(Post location) {
        MapsItemFragment f = new MapsItemFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_POST, location);
        f.setArguments(args);
        return f;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_maps_item, container, false);
        toolbar = (Toolbar) view.findViewById(R.id.toolbar);
        contentView = (WebView) view.findViewById(R.id.content);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) location = args.getParcelable(EXTRA_POST);

        ViewCompat.setElevation(getView(), 10f);
        ViewCompat.setElevation(toolbar, 4f);

        toolbar.setTitle(CommonUtils.stripHtml(location.getTitle().getRendered()));
        //toolbar.inflateMenu(R.menu.fragment);
        toolbar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DetailActivity.navigate(getActivity(), null, null, location);
            }
        });

        WebSettings settings = contentView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        Document doc = Jsoup.parse(location.getContent().getRendered());
        contentView.loadData(WebHelper.docToBetterHTML(doc, getActivity()), "text/html; charset=UTF-8", null);
        contentView.getSettings().setJavaScriptEnabled(true);
        contentView.setBackgroundColor(Color.argb(1, 0, 0, 0));
        contentView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        contentView.getSettings().setDefaultFontSize(WebHelper.getWebViewFontSize(getActivity()));
    }

}