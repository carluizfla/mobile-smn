package com.example.fresh.modulio.attachmentviewer.model;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class LocationAttachment extends Attachment {

    private Double lat;
    private Double lon;
    private String description;

    public LocationAttachment(Double lat, Double lon, String description){
        this.lat = lat;
        this.lon = lon;
        this.description = description;
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    @Override
    public String getDescription() {
        return description;
    }

}
