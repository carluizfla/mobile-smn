package com.example.fresh.modulio.menu;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.view.menu.MenuItemImpl;
import android.view.MenuItem;

import com.google.android.gms.maps.SupportMapFragment;

import java.io.Serializable;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class Action implements Serializable{

    public String name;
    public Class mClass;
    public String filterOn;
    public String filter;

    public String categoryImageUrl;

    private Action(Builder builder){
        this.mClass = builder.mClass;
        this.name = builder.name;
        this.filterOn = builder.filterOn;
        this.filter = builder.filter;
        this.categoryImageUrl = builder.categoryImageUrl;
    }

    public static class Builder {

        private String name;
        private Class mClass;
        private String filterOn;
        private String filter;

        private String categoryImageUrl;

        public Builder(@NonNull String name) {
            this.name = name;
        }

        public Builder withFragment(@NonNull Class mClass) {
            this.mClass = mClass;
            return this;
        }

        public Builder withFilter(String filterOn, @NonNull  String filter) {
            this.filter = filter;
            this.filterOn = filterOn;
            return this;
        }

        public Builder withCategoryImage(@NonNull String url){
            this.categoryImageUrl = url;
            return this;
        }

        public Action build() {
            return new Action(this);
        }

    }
}
