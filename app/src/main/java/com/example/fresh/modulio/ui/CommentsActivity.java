package com.example.fresh.modulio.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.afrozaar.wp_api_v2_client_android.model.Comment;
import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.model.WPGeneric;
import com.afrozaar.wp_api_v2_client_android.rest.HttpServerErrorResponse;
import com.afrozaar.wp_api_v2_client_android.rest.WordPressRestResponse;
import com.afrozaar.wp_api_v2_client_android.util.LoginAccountHelper;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.logic.UserUtils;
import com.example.fresh.modulio.WpClient;

import java.util.ArrayList;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class CommentsActivity extends AppCompatActivity {

    private FloatingActionButton fab;
    private Post post;

    /**
     * Show comments for a certain posts
     * @param context Context to launch activity
     * @param post The post to load comments for
     * @param commentList (optional) preload list of comments (page 1 only)
     */
    public static void startActivity(Context context, Post post, ArrayList<Comment> commentList){
        Intent intent = new Intent(context, CommentsActivity.class);
        intent.putExtra(CommentsListFragment.POST, post);
        intent.putExtra(CommentsListFragment.COMMENTS, commentList);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               showCommentDialog(0, post.getId());
            }
        });

        post = getIntent().getExtras().getParcelable(CommentsListFragment.POST);
        ArrayList<Comment> commentList = getIntent().getExtras().getParcelableArrayList(CommentsListFragment.COMMENTS);

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fragment, CommentsListFragment.newInstance(post, commentList));
        //ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.commit();
    }

    /**
     * Show a dialog to let the user comment to a post (and optionally another comment)
     * @param parentId Comment to reply to (0 if n/a)
     * @param postId Post to comment to
     */
    public void showCommentDialog(final long parentId, final long postId){
        if (!new UserUtils(this).isUserLoggedIn()){
            Snackbar.make(fab, R.string.comment_login, Snackbar.LENGTH_LONG)
                    .setAction(R.string.action_sign_in_short, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            startActivity(new Intent(CommentsActivity.this, LoginActivity.class));
                        }
                    }).show();

            return;
        }
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle(R.string.comment_dialog_title);

        //Load EditText for entry
        View mView = LayoutInflater.from(this).inflate(R.layout.activity_comments_add, null);
        alert.setView(mView);

        final EditText textInput = (EditText) mView.findViewById(R.id.userInputDialog);

        // Make an "OK" button to save the comment
        alert.setPositiveButton(R.string.comment_dialog_confirm, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int whichButton) {

                // Grab the EditText's input
                String input = textInput.getText().toString();

                // Create the commment
                Comment comment = new Comment()
                        .withPost(postId)
                        .withAuthor(Integer.parseInt(LoginAccountHelper.with(CommentsActivity.this).getUserId()))
                        .withAuthorName(LoginAccountHelper.with(CommentsActivity.this).getUserName())
                        .withContent(new WPGeneric().withRaw(input));

                if (parentId != 0){
                    comment.setParent(parentId);
                }

                //Submit it to the site
                WpClient.init(CommentsActivity.this).createComment(comment, new WordPressRestResponse<Comment>() {
                    @Override
                    public void onSuccess(Comment result) {
                        Snackbar.make(fab, R.string.comment_success, Snackbar.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(HttpServerErrorResponse errorResponse) {
                        Snackbar.make(fab, getString(R.string.comment_failure) +" (" + errorResponse.getCode() + ")", Snackbar.LENGTH_LONG).show();
                    }
                });


            }
        });

        // Make a "Cancel" button
        // that simply dismisses the alert
        alert.setNegativeButton(R.string.comment_dialog_cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {}
        });

        alert.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
