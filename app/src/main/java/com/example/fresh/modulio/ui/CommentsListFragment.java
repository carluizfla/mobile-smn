package com.example.fresh.modulio.ui;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.afrozaar.wp_api_v2_client_android.model.Comment;
import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.rest.HttpServerErrorResponse;
import com.afrozaar.wp_api_v2_client_android.rest.WordPressRestResponse;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemChildClickListener;
import com.example.fresh.modulio.adapter.CommentAdapter;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.WpClient;
import com.example.fresh.modulio.widgets.DividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class CommentsListFragment extends Fragment implements BaseQuickAdapter.RequestLoadMoreListener{

    public static String POST = "post";
    public static String COMMENTS = "comments";

    //Views
    private RecyclerView mRecyclerView;
    private View mEmptyView;
    private View mLoadingView;

    //List
    private ArrayList<Comment> items;
    private CommentAdapter commentsAdapter;

    //Paging
    private int PAGE_SIZE = 10;
    private int PAGE = 0;

    private Post post;
    private ArrayList<Comment> commentsListExtra;

    public static CommentsListFragment newInstance(Post post, ArrayList<Comment> comments) {
        CommentsListFragment lf = new CommentsListFragment();

        Bundle args = new Bundle();
        args.putParcelable(POST, post);
        args.putParcelableArrayList(COMMENTS, comments);
        lf.setArguments(args);

        return lf;
    }

    public CommentsListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_comments, container, false);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.rv_list);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(getActivity(), DividerItemDecoration.VERTICAL_LIST));
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        items = new ArrayList<>();

        mEmptyView = inflater.inflate(R.layout.list_empty_view, mRecyclerView, false);
        mLoadingView = view.findViewById(R.id.list_loading_view);

        commentsAdapter = new CommentAdapter(getActivity(), items);
        commentsAdapter.setOnLoadMoreListener(this);

        mRecyclerView.setAdapter(commentsAdapter);

        mRecyclerView.addOnItemTouchListener(new OnItemChildClickListener( ) {
            @Override
            public void SimpleOnItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                doReply(items.get(position).getId());
            }
        });

        //If some comments have been preloaded, simply load those, otherwise perform a request
        if (commentsListExtra != null){
            //If there are comments to load, load them.
            //Otherwise the user is here to make a comment himself so show the input dialog
            if (commentsListExtra.size() == 0) {
                doReply(0);
                loadCompleted();
            } else {
                processComments(commentsListExtra);
                PAGE = 1;
            }
        } else {
            onLoadMoreRequested();
        }

        commentsAdapter.openLoadMore(PAGE_SIZE);

        //Set the subtitle
        ((CommentsActivity) getActivity()).getSupportActionBar().setSubtitle(post.getTitle().getRendered());

        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        post = getArguments().getParcelable(POST);
        commentsListExtra = getArguments().getParcelableArrayList(COMMENTS);
    }

    @Override
    public void onLoadMoreRequested() {
        //Load the next page
        PAGE += 1;

        WpClient.init(getActivity()).getCommentsForPage(new WordPressRestResponse<List<Comment>>() {
            @Override
            public void onSuccess(List<Comment> comments) {
                processComments(comments);
            }

            @Override
            public void onFailure(HttpServerErrorResponse errorResponse) {
                loadCompleted();
            }
        }, post.getId(), PAGE);
    }

    private void loadCompleted(){
        //Hide the loading view if it isn't already hidden
        if (mLoadingView.getVisibility() == View.VISIBLE)
            mLoadingView.setVisibility(View.GONE);

        //Set the emptyview if it isn't set already
        if (commentsAdapter.getmEmptyViewCount() == 0)
            commentsAdapter.setEmptyView(mEmptyView);
    }

    public void doReply(long commentId) {
        ((CommentsActivity) getActivity()).showCommentDialog(commentId, post.getId());
    }

    private void processComments(List<Comment> comments){
        //Check if we can load more items
        boolean loadMore = (comments.size() == 10);

        //Add the comments to the list at the correct place
        for (Comment comment : comments){
            //It's a top level comment
            if (comment.getParent() == 0){
                items.add(comment);
            } else {
                long parentId = comment.getParent();
                for (int i = 0; i < items.size(); i++) {
                    //Place the comment below it's parent in the list
                    if (items.get(i).getId() == parentId){
                        items.add(i + 1, comment);
                        break;
                    }
                }
            }
        }

        //Notify the adapter
        if (!loadMore)
            commentsAdapter.loadComplete();

        commentsAdapter.dataAdded();

        loadCompleted();
    }

}
