package com.example.fresh.modulio.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.rest.HttpServerErrorResponse;
import com.afrozaar.wp_api_v2_client_android.rest.WordPressRestResponse;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.adapter.ItemAdapter;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.WpClient;
import com.example.fresh.modulio.adapter.TabAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class PrimaryFragment extends Fragment implements BaseQuickAdapter.RequestLoadMoreListener, SwipeRefreshLayout.OnRefreshListener, SearchView.OnQueryTextListener {

    //Views
    private RelativeLayout rl;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private View mEmptyView;
    private View mLoadingView;

    //Paging
    private int PAGE_SIZE = 10;
    private int PAGE;
    private String query;

    //List
    private ItemAdapter multipleItemAdapter;

    private ViewTreeObserver.OnGlobalLayoutListener recyclerListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rl = (RelativeLayout) inflater.inflate(R.layout.fragment_primary,null);
        setHasOptionsMenu(true);

        mRecyclerView = (RecyclerView) rl.findViewById(R.id.rv_list);

        final StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        recyclerListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //Get the view width, and check if it could be valid
                int viewWidth = mRecyclerView.getMeasuredWidth();
                if (viewWidth <= 0 ) return;

                //Remove the VTO
                mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                //Calculate and update the span
                float cardViewWidth = getResources().getDimension(R.dimen.card_width);
                int newSpanCount = Math.max(1, (int) Math.floor(viewWidth / cardViewWidth));
                mLayoutManager.setSpanCount(newSpanCount);
                mLayoutManager.requestLayout();
            }
        };
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(recyclerListener);

        mSwipeRefreshLayout = (SwipeRefreshLayout) rl.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        mEmptyView = inflater.inflate(R.layout.list_empty_view, mRecyclerView, false);
        mLoadingView = rl.findViewById(R.id.list_loading_view);

        multipleItemAdapter = new ItemAdapter(new ArrayList<Post>());
        multipleItemAdapter.setOnLoadMoreListener(this);
        multipleItemAdapter.openLoadMore(PAGE_SIZE);
        mRecyclerView.addOnItemTouchListener(new OnItemClickListener( ){
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                View title = view.findViewById(R.id.title);

                View thumbnail = view.findViewById(R.id.image);
                if (thumbnail == null || thumbnail.getVisibility() == View.GONE)
                    thumbnail = null;

                DetailActivity.navigate(getActivity(), thumbnail, title, multipleItemAdapter.getData().get(position));
            }
        });

        mRecyclerView.setAdapter(multipleItemAdapter);

        //Load items
        onLoadMoreRequested();

        return rl;
    }

    @Override
    public void onLoadMoreRequested() {
        //Load the next page
        PAGE += 1;

        //If a search query is specified, add it
        HashMap<String, String> params = new HashMap<>();
        if (query != null) {
            params.put("search", query);
        }

        params = CommonUtils.addFilters(params,
                getArguments().getString(TabAdapter.EXTRA_FILTERON),
                getArguments().getString(TabAdapter.EXTRA_FILTER));

        //Create a new client with the updated credientials and use it to get more info..
        WpClient.init(getActivity()).getPostsForPage(new WordPressRestResponse<List<Post>>() {
            @Override
            public void onSuccess(List<Post> posts) {
                //If this is the first page, we are coming from a reset, so clear the list first
                if (PAGE == 1){
                    multipleItemAdapter.getData().clear();
                }

                //Check if we can load more items
                boolean loadMore = (posts.size() == 10);
                if (!loadMore) multipleItemAdapter.loadComplete();

                //Add all the new posts to the list and notify the adapter
                multipleItemAdapter.addData(posts);

                loadCompleted();
            }

            @Override
            public void onFailure(HttpServerErrorResponse errorResponse) {
                loadCompleted();
            }
        }, PAGE, params);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(recyclerListener);
    }

    private void loadCompleted(){

        //Disable the swipe refresh layout
        if (mSwipeRefreshLayout.isRefreshing())
            mSwipeRefreshLayout.setRefreshing(false);

        //Hide the loading view if it isn't already hidden
        if (mLoadingView.getVisibility() == View.VISIBLE)
            mLoadingView.setVisibility(View.GONE);

        //Set the emptyview if it isn't set already
        if (multipleItemAdapter.getmEmptyViewCount() <= 0) {
            multipleItemAdapter.setEmptyView(mEmptyView);

            //The empty view is actually a list item, so notify the list that this item has been added
            multipleItemAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Resets the list
     * @param clearView Also visually empties the list if true
     */
    private void resetList(boolean clearView){
        //Reset
        PAGE = 0;

        if (clearView){
            multipleItemAdapter.getData().clear();
            multipleItemAdapter.notifyDataSetChanged();
            mLoadingView.setVisibility(View.VISIBLE);
            multipleItemAdapter.setEmptyView(null);
        }

        //(Re)-enable load more
        multipleItemAdapter.openLoadMore(PAGE_SIZE);
    }

    @Override
    public void onRefresh() {
        resetList(false);

        //Load items
        onLoadMoreRequested();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        final MenuItem item = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(item);
        searchView.setOnQueryTextListener(this);

        MenuItemCompat.setOnActionExpandListener(item,
                new MenuItemCompat.OnActionExpandListener() {
                    @Override
                    public boolean onMenuItemActionCollapse(MenuItem item) {
                        resetList(true);

                        //Remove the query
                        query = null;

                        //Load items
                        onLoadMoreRequested();

                        return true; // Return true to collapse action view
                    }

                    @Override
                    public boolean onMenuItemActionExpand(MenuItem item) {
                        // Do something when expanded
                        return true; // Return true to expand action view
                    }
                });
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        resetList(true);

        //Set the query;
        this.query = query;

        //Load items
        onLoadMoreRequested();

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

}
