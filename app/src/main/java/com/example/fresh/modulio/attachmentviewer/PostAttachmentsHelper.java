package com.example.fresh.modulio.attachmentviewer;

import android.util.Log;
import android.util.Pair;

import com.afrozaar.wp_api_v2_client_android.model.Media;
import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.util.DataConverters;
import com.example.fresh.modulio.Config;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.attachmentviewer.model.Attachment;
import com.example.fresh.modulio.attachmentviewer.model.LinkAttachment;
import com.example.fresh.modulio.attachmentviewer.model.LocationAttachment;
import com.example.fresh.modulio.attachmentviewer.model.MediaAttachment;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class PostAttachmentsHelper {

    //Keep track of attachments
    private Attachment mainAttachment;
    private List<Attachment> otherAttachments; //Does not include featured image and mainAttachment
    private MediaAttachment featuredImage;

    boolean featuredImageIsAlsoAttachedAndNotPrimary = false;

    public PostAttachmentsHelper(Post post){
        ArrayList<Attachment> attachments = new ArrayList<Attachment>();

        String featuredImageSource = null;
        if (post.getBetterFeaturedImage() != null) {
            featuredImage = (new MediaAttachment(
                    post.getBetterFeaturedImage(),
                    MediaAttachment.MIME_IMAGE));
            featuredImageSource = post.getBetterFeaturedImage().getSourceUrl();
        }

        for (Media media : post.getBetterAttachments()) {
            String mime;
            if (media.getMediaType().equals("image"))
                mime = MediaAttachment.MIME_IMAGE;
            else
                mime = media.getMediaDetails().getMimeType();

            //If it is not the featured image, and if a valid mime is provided, add it
            if (mime != null && !media.getSourceUrl().equals(featuredImageSource)) {
                attachments.add(new MediaAttachment(media, mime));
            } else if (media.getSourceUrl().equals(featuredImageSource)){
                featuredImageIsAlsoAttachedAndNotPrimary = true;
            } else {
                attachments.add(new MediaAttachment(media, MediaAttachment.MIME_FILE));
            }
        }

        if (DataConverters.parseSources(post.getSources()) != null)
            for (Pair<String,String> source : DataConverters.parseSources(post.getSources())){
                Attachment link = new LinkAttachment(source.second, source.first);
                attachments.add(link);
            }

        Pair<Double, Double> location = DataConverters.parseLocation(post.getLocation());
        if (location != null){
            Attachment link = new LocationAttachment(location.first, location.second, post.getTitle().getRendered());
            attachments.add(link);
        }

        otherAttachments = attachments;

        extractMainAttachment();
    }

    /**
     * Get a list of all the attachments of this posts
     * @param withFeaturedImage if true, the featured image is added to this list
     *                          given if it isn't already included as regular attachment
     * @return a list of all the attachments of this posts
     */
    public ArrayList<Attachment> getAttachments(boolean withFeaturedImage) {
        ArrayList<Attachment> attachments = new ArrayList<Attachment>();

        if (withFeaturedImage || featuredImageIsAlsoAttachedAndNotPrimary){
            //This is an override that can prevent the featured image from being included at all times
            if (!Config.FORCE_FEATURED_IMAGE_NEVER_ATTACHMENT)
            attachments.add(featuredImage);
        }

        if (otherAttachments != null)
            attachments.addAll(otherAttachments);

        return attachments;
    }

    private void extractMainAttachment(){
        //If we only have 1 attachment, it is the main attachment
        if (otherAttachments.size() == 1){
             mainAttachment = otherAttachments.get(0);
        } else if (otherAttachments.size() > 1){
            //If there is an outstanding attachment, it's the main attachment
            Attachment outstandingAttachment = onlyImageOrVideoButOne();
            if (outstandingAttachment != null){
                mainAttachment = outstandingAttachment;
            }
        } else if (featuredImage != null){
            mainAttachment = featuredImage;
            featuredImageIsAlsoAttachedAndNotPrimary = false;
        }

        otherAttachments.remove(mainAttachment);
    }

    public Attachment getMainAttachment(){
        return mainAttachment;
    }

    /**
     * Get the single attachment that is not a video nor an image.
     * @return The attachment that is not a video or image. Null if there are none or multiple
     */
    public Attachment onlyImageOrVideoButOne() {
        return onlyImageOrVideoButOne(false);
    }

    /**
     * Check if all 'otherAttachments' but 1 are images or videos and return it.
     * @param videoAsOther if true treat a video as any non-image (other) attachment.
     * @return The attachment that is 'other'. Null if there is not a single 'other'
     */
    public Attachment onlyImageOrVideoButOne(boolean videoAsOther) {
        int videoCount = 0;
        int otherCount = 0; //nor video nor image

        Attachment otherAttachment = null;

        for (Attachment attachment : otherAttachments) {
            //If the attachment is not media, or media but not an image
            if (!(attachment instanceof MediaAttachment)
                    || !((MediaAttachment) attachment).getMime().equals(MediaAttachment.MIME_IMAGE)) {
                //If the attachment is media and and a video
                if ((attachment instanceof MediaAttachment) &&
                        ((MediaAttachment) attachment).getMime().startsWith(MediaAttachment.MIME_PATTERN_VID)) {
                    videoCount++;

                    if (videoAsOther)
                        otherAttachment = attachment;
                //If the attachment is not a video or not media
                } else {
                    otherCount++;
                    otherAttachment = attachment;
                }
            }
        }

        if (videoAsOther) {
            if ((otherCount + videoCount) == 1)
                return otherAttachment;
            else
                return null;
        } else {
            if (otherCount == 1)
                return otherAttachment;
            else
                return null;
        }

    }

    public static int getAttachmentIcon(Attachment attachment){
        //If we only have an audio attachment
        if (attachment instanceof MediaAttachment
                && ((MediaAttachment) attachment).getMime().startsWith(MediaAttachment.MIME_PATTERN_AUDIO)){
            return R.drawable.ic_av_play_arrow;
            //If we only have a video attachment
        } else if (attachment instanceof MediaAttachment
                && ((MediaAttachment) attachment).getMime().startsWith(MediaAttachment.MIME_PATTERN_VID)) {
            return (R.drawable.ic_av_play_arrow);
            //If we only have a image attachment
        } else if (attachment instanceof MediaAttachment
                && ((MediaAttachment) attachment).getMime().equals(MediaAttachment.MIME_IMAGE)) {
            return (R.drawable.ic_photo);
            //If we only have one mediaattachment, that is nor a video, image or audio file show a download button
        } else if (attachment instanceof MediaAttachment) {
            return (R.drawable.ic_download);
            //If we only have one attachment, that is a link attachment
        } else if (attachment instanceof LinkAttachment) {
            return (R.drawable.ic_web);
            //If we only have one attachment, that is a location attachment
        } else if (attachment instanceof LocationAttachment) {
            return (R.drawable.ic_location);
        } else {
            throw new RuntimeException("Inconsistency! All use Attachments should be covered by the above statement");
        }
    }

}
