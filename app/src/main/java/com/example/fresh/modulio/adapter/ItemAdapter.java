package com.example.fresh.modulio.adapter;

import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.Config;
import com.example.fresh.modulio.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */

public class ItemAdapter extends BaseQuickAdapter<Post> {
    public static final int TEXT_TYPE = 1;
    public static final int CARD_TYPE = 2;
    public static final int PRIMARY_CARD_TYPE = 3;
    public static final int IMAGE_TYPE = 4;

    public static final int AUTO = 0;

    public ItemAdapter(List<Post> data ) {
        super( data);
    }

    @Override
    protected int getDefItemViewType(int position) {

        if (position >= 0 && position < getData().size()){
            //Override the item type from Config
            if (Config.FORCE_ROW_STYLE != AUTO) return Config.FORCE_ROW_STYLE;

            if (CommonUtils.stripHtml(getData().get(position).getContent().getRendered()).equals("")
                && getData().get(position).getBetterFeaturedImage() != null)
                return IMAGE_TYPE;
            if (getData().get(position).getBetterFeaturedImage() != null) {
                if (position == 0 && !Config.NEVER_FEATURE_CARD) return PRIMARY_CARD_TYPE;
                return CARD_TYPE;
            } else
                return TEXT_TYPE;
        }
        return super.getDefItemViewType(position);
    }

    @Override
    protected BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        if (viewType == CARD_TYPE)
            return new CardViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_image_text, parent, false));
        else if (viewType == TEXT_TYPE)
            return new TextViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_image_text, parent, false));
        else if (viewType == IMAGE_TYPE)
            return new ImageViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_image, parent, false));
        else if (viewType == PRIMARY_CARD_TYPE)
            return new PrimaryCardViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_featured, parent, false));

        //Return default, e.g. empty view or loading view
        return super.onCreateDefViewHolder(parent, viewType);
    }

    @Override
    protected void convert(final BaseViewHolder baseViewHolder, Post post) {
        if (baseViewHolder instanceof CardViewHolder) {

            Picasso.with(baseViewHolder.getConvertView().getContext())
                    .load(post.getBetterFeaturedImage().getGoodSourceUrl())
                    .placeholder(R.drawable.placeholder)
                    .into(((CardViewHolder) baseViewHolder).image);
            ((CardViewHolder) baseViewHolder).subtitle.setText(CommonUtils.stripHtml(post.getContent().getRendered()).toString().trim());
            ((CardViewHolder) baseViewHolder).title.setText(CommonUtils.stripHtml(post.getTitle().getRendered()));

        } else if (baseViewHolder instanceof TextViewHolder) {

            ((TextViewHolder) baseViewHolder).subtitle.setText(CommonUtils.stripHtml(post.getContent().getRendered()).toString().trim());
            ((TextViewHolder) baseViewHolder).title.setText(CommonUtils.stripHtml(post.getTitle().getRendered()));

        }  else if (baseViewHolder instanceof ImageViewHolder) {

            Picasso.with(baseViewHolder.getConvertView().getContext())
                    .load(post.getBetterFeaturedImage().getGoodSourceUrl())
                    .placeholder(R.drawable.placeholder)
                    .into(((ImageViewHolder) baseViewHolder).image);
            ((ImageViewHolder) baseViewHolder).title.setText(CommonUtils.stripHtml(post.getTitle().getRendered()));

            //Set to span full width if it's featured
            if (baseViewHolder instanceof PrimaryCardViewHolder) {
                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) baseViewHolder.itemView.getLayoutParams();
                layoutParams.setFullSpan(true);
            }

        }
    }

    public class PrimaryCardViewHolder extends ImageViewHolder {

        public PrimaryCardViewHolder(View itemView) {
            super(itemView);
        }
    }

    public class ImageViewHolder extends BaseViewHolder {
        public TextView title;
        public ImageView image;

        public ImageViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }


    public class CardViewHolder extends TextViewHolder {
        public ImageView image;

        public CardViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
            itemView.findViewById(R.id.image).setVisibility(View.VISIBLE);
        }
    }

    public class TextViewHolder extends BaseViewHolder {
        public TextView title;
        public TextView subtitle;

        public TextViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            subtitle = (TextView) itemView.findViewById(R.id.subtitle);
        }
    }


}