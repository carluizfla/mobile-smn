package com.example.fresh.modulio.ui;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.ShareActionProvider;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.support.v7.graphics.Palette;
import android.transition.Slide;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrozaar.wp_api_v2_client_android.model.Comment;
import com.afrozaar.wp_api_v2_client_android.model.Post;
import com.afrozaar.wp_api_v2_client_android.rest.HttpServerErrorResponse;
import com.afrozaar.wp_api_v2_client_android.rest.WordPressRestResponse;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.example.fresh.modulio.Config;
import com.example.fresh.modulio.logic.ChromeHelper;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.adapter.AttachmentAdapter;
import com.example.fresh.modulio.adapter.ItemAdapter;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.logic.WebHelper;
import com.example.fresh.modulio.WpClient;
import com.example.fresh.modulio.attachmentviewer.model.Attachment;
import com.example.fresh.modulio.attachmentviewer.model.LinkAttachment;
import com.example.fresh.modulio.attachmentviewer.model.LocationAttachment;
import com.example.fresh.modulio.attachmentviewer.model.MediaAttachment;
import com.example.fresh.modulio.attachmentviewer.PostAttachmentsHelper;
import com.example.fresh.modulio.attachmentviewer.ui.AttachmentActivity;
import com.example.fresh.modulio.attachmentviewer.ui.AudioPlayerActivity;
import com.example.fresh.modulio.attachmentviewer.ui.VideoPlayerActivity;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class DetailActivity extends AppCompatActivity  {

    private static final String EXTRA_VIEW_IMAGE = "extraViewImage";
    private static final String EXTRA_VIEW_TITLE = "extraViewTitle";
    private static final String EXTRA_POST = "extraPost";

    //Views
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private AppBarLayout appBarLayout;
    private CoordinatorLayout coordinatorLayout;
    private FloatingActionButton fab;

    private Post post;
    private ArrayList<Comment> commentsList;
    PostAttachmentsHelper attachmentsHelper;

    //Related list
    private RecyclerView mRecyclerView;
    private ItemAdapter relatedItemAdapter;

    //Attachments list
    private RecyclerView mAttachmentList;
    private AttachmentAdapter attachmentAdapter;

    public static void navigate(Activity activity, View imageView, View titleView, Post viewModel) {
        Intent intent = new Intent(activity, DetailActivity.class);
        intent.putExtra(EXTRA_POST, viewModel);

        //In case the user has configured to not show the detailview but skip to the attachment viewer
        if (Config.SKIP_DETAILVIEW_IF_ATTACHMENT){
            PostAttachmentsHelper attHelper = new PostAttachmentsHelper(viewModel);
            final Attachment mainAttachment = attHelper.getMainAttachment();

            if (attHelper.getAttachments(false).size() == 0 && mainAttachment == null){
                //If we have zero attachments to show
                //Continue loading as normally
            } else {
                if (mainAttachment != null) {
                    handleAttachmentClick(mainAttachment, activity);
                } else {
                    AttachmentActivity.startActivity(activity, attHelper.getAttachments(true));
                }
                return;
            }
        }

        ActivityOptionsCompat options;
        //if (imageView != null && !Config.SKIP_DETAILVIEW_IF_ATTACHMENT) {
        //    android.support.v4.util.Pair<View, String> imagePair = android.support.v4.util.Pair.create(imageView, EXTRA_VIEW_IMAGE);
        ///    android.support.v4.util.Pair<View, String> titlePair = android.support.v4.util.Pair.create(titleView, EXTRA_VIEW_TITLE);
        //    options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity, imagePair, titlePair);
        //} else {
            options = ActivityOptionsCompat.makeSceneTransitionAnimation(activity);
        //}
        ActivityCompat.startActivity(activity, intent, options.toBundle());
    }

    @SuppressWarnings("ConstantConditions")
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initActivityTransitions();
        setContentView(R.layout.activity_detail);

        //ViewCompat.setTransitionName(findViewById(R.id.header_image), EXTRA_VIEW_IMAGE);
        //supportPostponeEnterTransition();

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        post = getIntent().getParcelableExtra(EXTRA_POST);
        attachmentsHelper = new PostAttachmentsHelper(post);

        CharSequence title = CommonUtils.stripHtml(post.getTitle().getRendered());

        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);
        collapsingToolbarLayout.setTitle(title);
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(android.R.color.transparent));

        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator_layout);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv_list);
        mAttachmentList = (RecyclerView) findViewById(R.id.attachment_list);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        setupRelatedList();
        setupComments();
        setupAttachmentList();
        setupFAB();
        CommonUtils.loadAdmob(findViewById(R.id.adView));

        //Adjust the layout to the featured image (or not)
        if (post.getBetterFeaturedImage() != null) {
            //Load the featured image
            final ImageView image = (ImageView) findViewById(R.id.header_image);
            Picasso.with(this).load(post.getBetterFeaturedImage().getGoodSourceUrl())
                    .into(image, new Callback() {
                @Override
                public void onSuccess() {
                    Bitmap bitmap = ((BitmapDrawable) image.getDrawable()).getBitmap();
                    Palette.from(bitmap).generate(new Palette.PaletteAsyncListener() {
                        public void onGenerated(Palette palette) {
                            applyPalette(palette);
                        }
                    });
                }

                @Override
                public void onError() {

                }
            });

            //If a fling upwards is made, collapse the appbarlayout
            ((NestedScrollView) findViewById(R.id.scroll)).setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
                @Override
                public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {

                    if(scrollY==0 && oldScrollY - scrollY > 100)
                    {
                        appBarLayout.setExpanded(true);
                    }
                }
            });

            //Align the fab to the collapsingtoolbar
            CoordinatorLayout.LayoutParams p = (CoordinatorLayout.LayoutParams) fab.getLayoutParams();
            p.setAnchorId(appBarLayout.getId());
            p.anchorGravity = Gravity.BOTTOM | GravityCompat.END;
            p.gravity = 0;
            fab.setLayoutParams(p);

        } else {
            final View toolbar = findViewById(R.id.toolbar);
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorPrimary));

            //Replace the collapsing toolbar with the normal toolbar
            collapsingToolbarLayout.removeAllViews();
            appBarLayout.removeAllViews();
            coordinatorLayout.removeView(findViewById(R.id.app_bar_layout));
            coordinatorLayout.addView(toolbar);

            //Set the status bar color
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
            }

            //Adjust the content to be below the toolbar
            ViewTreeObserver obs = toolbar.getViewTreeObserver();
            obs.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    appBarLayout.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                    int toppadding = toolbar.getMeasuredHeight();
                    findViewById(R.id.scrolling).setPadding(0, toppadding, 0, 0);
                }
            });
        }

        TextView titleView = (TextView) findViewById(R.id.title);
        titleView.setText(title);

        TextView date = (TextView) findViewById(R.id.date);
        date.setText(CommonUtils.relativeDateFromWp(this, post.getDate()));

        WebView contentView = (WebView) findViewById(R.id.content);
        WebSettings settings = contentView.getSettings();
        settings.setDefaultTextEncodingName("utf-8");
        Document doc = Jsoup.parse(post.getContent().getRendered());
        contentView.loadData(WebHelper.docToBetterHTML(doc, this), "text/html; charset=UTF-8", null);
        contentView.getSettings().setJavaScriptEnabled(true);
        contentView.setBackgroundColor(Color.argb(1, 0, 0, 0));
        contentView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        contentView.getSettings().setDefaultFontSize(WebHelper.getWebViewFontSize(this));
        contentView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                if (url != null
                        && (url.endsWith(".png") || url
                        .endsWith(".jpg")|| url
                        .endsWith(".jpeg"))) {
                    ArrayList<Attachment> attachments = new ArrayList<Attachment>();
                    attachments.add(new MediaAttachment(url, MediaAttachment.MIME_IMAGE, null));
                    AttachmentActivity.startActivity(DetailActivity.this, attachments);
                    return true;
                } else if (url != null && (url.startsWith("http://") || url.startsWith("https://"))) {
                    ChromeHelper.openUrl(url, DetailActivity.this);
                    return true;
                } else {
                    Uri uri = Uri.parse(url);
                    Intent ViewIntent = new Intent(Intent.ACTION_VIEW, uri);

                    // Verify it resolves
                    PackageManager packageManager = getPackageManager();
                    List<ResolveInfo> activities = packageManager.queryIntentActivities(ViewIntent, 0);
                    boolean isIntentSafe = activities.size() > 0;

                    // Start an activity if it's safe
                    if (isIntentSafe) {
                        startActivity(ViewIntent);
                    }
                    return true;
                }
            }
        });
    }

    private void hideRelatedList(){
        findViewById(R.id.related).setVisibility(View.GONE);
    }

    private void setupRelatedList(){
        mRecyclerView.setNestedScrollingEnabled(false);

        if (post.getTags().size() == 0) {
            hideRelatedList();
            return;
        };

        final StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(
                new ViewTreeObserver.OnGlobalLayoutListener() {
                    @Override
                    public void onGlobalLayout() {
                        mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        int viewWidth = mRecyclerView.getMeasuredWidth();
                        float cardViewWidth = getResources().getDimension(R.dimen.card_width);
                        int newSpanCount = (int) Math.floor(viewWidth / cardViewWidth);
                        mLayoutManager.setSpanCount(newSpanCount);
                        mLayoutManager.requestLayout();
                    }
                });

        relatedItemAdapter = new ItemAdapter(new ArrayList<Post>());
        mRecyclerView.addOnItemTouchListener(new OnItemClickListener( ){
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                View title = view.findViewById(R.id.title);
                View thumbnail = view.findViewById(R.id.image);
                if (thumbnail.getVisibility() == View.GONE)
                    thumbnail = null;

                DetailActivity.navigate(DetailActivity.this, thumbnail, title, relatedItemAdapter.getData().get(position));
            }
        });
        mRecyclerView.setAdapter(relatedItemAdapter);
        //Create a new client with the updated credentials and use it to get more info..
        WpClient.init(this).getPostsForTag(post.getTags().get(0), new WordPressRestResponse<List<Post>>() {
            @Override
            public void onSuccess(List<Post> posts) {

                //Remove self
                for (Iterator<Post> iter = posts.iterator(); iter.hasNext(); ) {
                    if (iter.next().getId() == post.getId())
                        iter.remove();
                }

                //If the list is empty, cancel
                if (posts.size() == 0) {
                    hideRelatedList();
                    return;
                }

                //Only show 3 related items
                if (posts.size() > 3)
                    posts.subList(2 , posts.size()).clear();

                //Add all the new posts to the list and notify the adapter
                relatedItemAdapter.addData(posts);
            }

            @Override
            public void onFailure(HttpServerErrorResponse errorResponse) {
            }
        });
    }

    private void setupComments(){
        final Button commentsButton = (Button) findViewById(R.id.commentsButton);
        commentsList = null;

        //Create a new client with the updated credientials and use it to get more info..
        WpClient.init(this).getComments(new WordPressRestResponse<List<Comment>>() {
            @Override
            public void onSuccess(List<Comment> comments) {
                if (comments.size() > 0) {
                    ViewStub stub = (ViewStub) findViewById(R.id.layout_comment_stub);
                    stub.setLayoutResource(R.layout.fragment_comments_comment);
                    View inflated = stub.inflate();

                    Picasso.with(DetailActivity.this)
                            .load(comments.get(0).getAuthorAvatarUrl("96"))
                            .into((ImageView) inflated.findViewById(R.id.avatarView));

                    ((TextView) inflated.findViewById(R.id.userView))
                            .setText(comments.get(0).getAuthorName());

                    ((TextView) inflated.findViewById(R.id.contentView))
                            .setText(CommonUtils.stripHtml(comments.get(0).getContent().getRendered()));

                    ((TextView) inflated.findViewById(R.id.dateView))
                            .setText(CommonUtils.relativeDateFromWp(DetailActivity.this, comments.get(0).getDate()));

                    inflated.findViewById(R.id.replyButton).setVisibility(View.GONE);
                } else {
                    commentsButton.setText(getResources().getString(R.string.comments_open_none));
                }

                if (comments instanceof ArrayList)
                    commentsList = (ArrayList<Comment>) comments;
                else
                    commentsList = new ArrayList<>(commentsList);
            }

            @Override
            public void onFailure(HttpServerErrorResponse errorResponse) {

            }
        }, post.getId());

        //Set the 'see all' comments button action
        commentsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CommentsActivity.startActivity(DetailActivity.this, post, commentsList);
            }
        });
    }

    private void setupAttachmentList(){
        mAttachmentList.setNestedScrollingEnabled(false);

        if (attachmentsHelper.getAttachments(false).size() == 0) {
            findViewById(R.id.attachments).setVisibility(View.GONE);
            return;
        };

        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mAttachmentList.setLayoutManager(mLayoutManager);

        attachmentAdapter = new AttachmentAdapter(attachmentsHelper.getAttachments(false));
        mAttachmentList.addOnItemTouchListener(new OnItemClickListener(){
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter baseQuickAdapter, View view, int i) {
                Attachment attachment = attachmentsHelper.getAttachments(false).get(i);
                handleAttachmentClick(attachment, DetailActivity.this);
            }
        });

        mAttachmentList.setAdapter(attachmentAdapter);

    }

    private void setupFAB(){
        final Attachment mainAttachment = attachmentsHelper.getMainAttachment();

        //If we have zero attachments to show
        if (attachmentsHelper.getAttachments(false).size() == 0 && mainAttachment == null){
            fab.setVisibility(View.GONE);
            return;
        }

        if (mainAttachment != null) {
            fab.setImageResource(PostAttachmentsHelper.getAttachmentIcon(mainAttachment));
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    handleAttachmentClick(mainAttachment, DetailActivity.this);
                }
            });
        } else {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AttachmentActivity.startActivity(DetailActivity.this, attachmentsHelper.getAttachments(true));
                }
            });
        }
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent motionEvent) {
        try {
            return super.dispatchTouchEvent(motionEvent);
        } catch (NullPointerException e) {
            return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                ActivityCompat.finishAfterTransition(this);
                return true;
            case R.id.menu_item_share:
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_SEND);
                intent.setType("text/plain");
                intent.putExtra(Intent.EXTRA_TEXT, post.getTitle().getRendered() + " \n" + post.getGuid().getRendered());
                startActivity(Intent.createChooser(intent, getString(R.string.share_with)));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate menu resource file.
        getMenuInflater().inflate(R.menu.menu_share, menu);

        // Return true to display menu
        return true;
    }

    private static void handleAttachmentClick(Attachment attachment, Activity source){
        //If we only have an audio attachment
        if (attachment instanceof MediaAttachment
                && ((MediaAttachment) attachment).getMime().startsWith(MediaAttachment.MIME_PATTERN_AUDIO)){

            AudioPlayerActivity.startActivity(source, ((MediaAttachment) attachment).getUrl(), ((MediaAttachment) attachment).getDescription());
            //If we only have a video attachment
        } else if (attachment instanceof MediaAttachment
                && ((MediaAttachment) attachment).getMime().startsWith(MediaAttachment.MIME_PATTERN_VID)) {

            VideoPlayerActivity.startActivity(source, ((MediaAttachment) attachment).getUrl());
            //If we only have a image attachment
        } else if (attachment instanceof MediaAttachment
                && ((MediaAttachment) attachment).getMime().equals(MediaAttachment.MIME_IMAGE)) {
            ArrayList<Attachment> singleMap = new ArrayList<Attachment>();
            singleMap.add(attachment);
            AttachmentActivity.startActivity(source, singleMap);
            //If we only have one mediaattachment, that is nor a video, image or audio file show a download button
        } else if (attachment instanceof MediaAttachment) {
            CommonUtils.download(source, ((MediaAttachment) attachment).getUrl());
            //If we only have one attachment, that is a link attachment
        } else if (attachment instanceof LinkAttachment) {
            ChromeHelper.openUrl(((LinkAttachment) attachment).getUrl(), source);
            //If we only have one attachment, that is a location attachment
        } else if (attachment instanceof LocationAttachment) {
            ArrayList<Attachment> singleMap = new ArrayList<Attachment>();
            singleMap.add(attachment);
            AttachmentActivity.startActivity(source, singleMap);
        } else {
            throw new RuntimeException("Inconsistency! All use Attachments should be covered by the above statement");
        }
    }

    private void initActivityTransitions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Slide transition = new Slide();
            transition.excludeTarget(android.R.id.statusBarBackground, true);
            getWindow().setEnterTransition(transition);
            getWindow().setReturnTransition(transition);
        }
    }

    private void applyPalette(Palette palette) {
        int primaryDark = getResources().getColor(R.color.colorPrimaryDark);
        int primary = getResources().getColor(R.color.colorPrimary);
        collapsingToolbarLayout.setContentScrimColor(palette.getMutedColor(primary));
        collapsingToolbarLayout.setStatusBarScrimColor(palette.getDarkMutedColor(primaryDark));
        //supportStartPostponedEnterTransition();
    }

    /**
     * Get status bar height if it is part of the layout (e.g. when it is transparent and shows part of our layout on Android Lollipop)
     * @return height of the status bar
     */
    public int getLayoutStatusBarHeight() {
        int result = 0;
        int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0 && (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT)) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }
}
