package com.example.fresh.modulio.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.menu.Action;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */

public class CategoryAdapter extends BaseQuickAdapter<Action> {
    private static final int TEXT_TYPE = 0;
    private static final int IMAGE_TYPE = 1;

    public CategoryAdapter(List<Action> data ) {
        super( data);
    }

    @Override
    protected int getDefItemViewType(int position) {
        if (position >= 0 && position < getData().size()){
            if (getData().get(position).categoryImageUrl != null && !getData().get(position).categoryImageUrl.isEmpty())
                return IMAGE_TYPE;
            else
                return TEXT_TYPE;
        }
        return super.getDefItemViewType(position);
    }

    @Override
    protected BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TEXT_TYPE)
            return new TextViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_image_text, parent, false));
        else if (viewType == IMAGE_TYPE)
            return new ImageViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_image, parent, false));

        //Return default, e.g. empty view or loading view
        return super.onCreateDefViewHolder(parent, viewType);
    }

    @Override
    protected void convert(final BaseViewHolder baseViewHolder, Action action) {
        if (baseViewHolder instanceof TextViewHolder) {

            ((TextViewHolder) baseViewHolder).title.setText(action.name);

        }  else if (baseViewHolder instanceof ImageViewHolder) {

            Picasso.with(baseViewHolder.getConvertView().getContext())
                    .load(action.categoryImageUrl)
                    .placeholder(R.drawable.placeholder)
                    .into(((ImageViewHolder) baseViewHolder).image);
            ((ImageViewHolder) baseViewHolder).title.setText(action.name);

        }
    }

    public class ImageViewHolder extends BaseViewHolder {
        public TextView title;
        public ImageView image;

        public ImageViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public class TextViewHolder extends BaseViewHolder {
        public TextView title;

        public TextViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            itemView.findViewById(R.id.subtitle).setVisibility(View.GONE);
        }
    }


}