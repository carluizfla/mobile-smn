package com.example.fresh.modulio.ui;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.listener.OnItemClickListener;
import com.example.fresh.modulio.logic.OverviewParser;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.adapter.CategoryAdapter;
import com.example.fresh.modulio.adapter.TabAdapter;
import com.example.fresh.modulio.menu.Action;

import java.util.ArrayList;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class OverviewFragment extends Fragment {

    //Views
    private RelativeLayout rl;
    private RecyclerView mRecyclerView;
    private View mEmptyView;
    private View mLoadingView;

    //List
    private CategoryAdapter multipleItemAdapter;

    private ViewTreeObserver.OnGlobalLayoutListener recyclerListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rl = (RelativeLayout) inflater.inflate(R.layout.fragment_primary,null);
        setHasOptionsMenu(true);
        mRecyclerView = (RecyclerView) rl.findViewById(R.id.rv_list);

        final StaggeredGridLayoutManager mLayoutManager = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(mLayoutManager);

        rl.findViewById(R.id.swiperefresh).setEnabled(false);

        recyclerListener = new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                //Get the view width, and check if it could be valid
                int viewWidth = mRecyclerView.getMeasuredWidth();
                if (viewWidth <= 0 ) return;

                //Remove the VTO
                mRecyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                //Calculate and update the span
                float cardViewWidth = getResources().getDimension(R.dimen.card_width_overview);
                int newSpanCount = Math.max(1, (int) Math.floor(viewWidth / cardViewWidth));
                mLayoutManager.setSpanCount(newSpanCount);
                mLayoutManager.requestLayout();
            }
        };
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(recyclerListener);

        mEmptyView = inflater.inflate(R.layout.list_empty_view, mRecyclerView, false);
        mLoadingView = rl.findViewById(R.id.list_loading_view);

        multipleItemAdapter = new CategoryAdapter(new ArrayList<Action>());
        mRecyclerView.addOnItemTouchListener(new OnItemClickListener( ){
            @Override
            public void SimpleOnItemClick(BaseQuickAdapter adapter, View view, int position) {
                HolderActivity.startActivity(multipleItemAdapter.getItem(position), getActivity());
            }
        });

        mRecyclerView.setAdapter(multipleItemAdapter);

        //Load items
        loadItems();

        return rl;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(getActivity(), SettingsActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void loadItems() {
        new OverviewParser(getArguments().getString(TabAdapter.EXTRA_FILTER), getActivity(), new OverviewParser.CallBack() {
            @Override
            public void categoriesLoaded(ArrayList<Action> result, boolean failed) {
                if (failed) return;

                //Add all the new posts to the list and notify the adapter
                multipleItemAdapter.addData(result);
                multipleItemAdapter.notifyDataSetChanged();
            }
        }).execute();


        loadCompleted();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mRecyclerView.getViewTreeObserver().addOnGlobalLayoutListener(recyclerListener);
    }

    private void loadCompleted(){

        //Hide the loading view if it isn't already hidden
        if (mLoadingView.getVisibility() == View.VISIBLE)
            mLoadingView.setVisibility(View.GONE);

        //Set the emptyview if it isn't set already
        if (multipleItemAdapter.getmEmptyViewCount() <= 0) {
            multipleItemAdapter.setEmptyView(mEmptyView);

            //The empty view is actually a list item, so notify the list that this item has been added
            multipleItemAdapter.notifyDataSetChanged();
        }
    }

}
