package com.example.fresh.modulio.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.afrozaar.wp_api_v2_client_android.model.Media;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.attachmentviewer.PostAttachmentsHelper;
import com.example.fresh.modulio.attachmentviewer.model.Attachment;
import com.example.fresh.modulio.attachmentviewer.model.MediaAttachment;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */

public class AttachmentAdapter extends BaseQuickAdapter<Attachment> {
    private static final int TYPE_IMAGE = 0;
    private static final int TYPE_OTHER = 1;

    public AttachmentAdapter(List<Attachment> data ) {
        super( data);
    }

    @Override
    protected int getDefItemViewType(int position) {
        if (position >= 0 && position < getData().size()){
            if (getData().get(position) instanceof MediaAttachment
                    && ((MediaAttachment) getData().get(position)).getMime().equals(MediaAttachment.MIME_IMAGE))
                return TYPE_IMAGE;
            else
                return TYPE_OTHER;
        }
        return super.getDefItemViewType(position);
    }

    @Override
    protected BaseViewHolder onCreateDefViewHolder(ViewGroup parent, int viewType) {
        BaseViewHolder holder = null;
        if (viewType == TYPE_IMAGE)
            return new ImageViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.attachment_card_image, parent, false));
        else if (viewType == TYPE_OTHER)
            return new OtherViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.attachment_card_action, parent, false));

        //Return default, e.g. empty view or loading view
        return super.onCreateDefViewHolder(parent, viewType);
    }

    @Override
    protected void convert(final BaseViewHolder baseViewHolder, Attachment attachment) {
        if (baseViewHolder instanceof ImageViewHolder) {
            assert attachment instanceof MediaAttachment;

            Picasso.with(baseViewHolder.getConvertView().getContext()).load(((MediaAttachment) attachment).getUrl()).into(((ImageViewHolder) baseViewHolder).image);

        } else if (baseViewHolder instanceof OtherViewHolder) {

            String description = "";
            if (attachment.getDescription() != null && !attachment.getDescription().equals(""))
                description = attachment.getDescription();
            else if (attachment instanceof MediaAttachment &&
                    ((MediaAttachment) attachment).getMime().equals(MediaAttachment.MIME_FILE))
                description = mContext.getString(R.string.file);

            ((OtherViewHolder) baseViewHolder).action.setImageResource(PostAttachmentsHelper.getAttachmentIcon(attachment));
            ((OtherViewHolder) baseViewHolder).title.setText(description);

        }
    }

    public class ImageViewHolder extends BaseViewHolder {
        public ImageView image;

        public ImageViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image);
        }
    }

    public class OtherViewHolder extends BaseViewHolder {
        public ImageView action;
        public TextView title;

        public OtherViewHolder(View itemView) {
            super(itemView);
            action = (ImageView) itemView.findViewById(R.id.action);
            title = (TextView) itemView.findViewById(R.id.title);
        }
    }


}