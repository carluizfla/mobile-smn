package com.example.fresh.modulio.attachmentviewer.ui;

import android.app.WallpaperManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.R;
import com.example.fresh.modulio.attachmentviewer.model.LocationAttachment;
import com.example.fresh.modulio.attachmentviewer.model.MediaAttachment;
import com.example.fresh.modulio.attachmentviewer.loader.GoogleMapsLoader;
import com.example.fresh.modulio.attachmentviewer.widgets.HackyViewPager;
import com.example.fresh.modulio.attachmentviewer.loader.MediaLoader;
import com.example.fresh.modulio.attachmentviewer.widgets.ScrollGalleryView;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;

import uk.co.senab.photoview.PhotoViewAttacher;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class AttachmentFragment extends Fragment {

    //Constants
    public static final String MEDIALOADER = "loader";
    public static final String ZOOM = "zoom";

    //Media to load
    private MediaLoader mMediaLoader;

    //Views
    private HackyViewPager viewPager;
    private ImageView backgroundImage;
    private TextView descriptionView;
    private ScrollGalleryView scrollGalleryView;

    private PhotoViewAttacher photoViewAttacher;

    public void setMediaLoader(MediaLoader mediaLoader) {
        mMediaLoader = mediaLoader;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        View rootView = inflater.inflate(R.layout.fragment_attachment, container, false);
        backgroundImage = (ImageView) rootView.findViewById(R.id.backgroundImage);
        descriptionView = (TextView) rootView.findViewById(R.id.description);
        viewPager = (HackyViewPager) getActivity().findViewById(R.id.viewPager);
        scrollGalleryView = (ScrollGalleryView) getActivity().findViewById(R.id.scroll_gallery_view);

        if (savedInstanceState != null) {
            mMediaLoader = (MediaLoader) savedInstanceState.getSerializable(MEDIALOADER);
        }

        loadMediaToView(rootView, savedInstanceState);

        String description = mMediaLoader.getAttachment().getDescription();
        if (description != null && !description.isEmpty()) {
            descriptionView.setText(CommonUtils.stripHtml(description));
            descriptionView.setVisibility(View.VISIBLE);
        }

        if (scrollGalleryView.thumbnailsHidden()){
            rootView.findViewById(R.id.thumbnail_container_padding).setVisibility(View.GONE);
        }

        return rootView;
    }

    private void loadMediaToView(View rootView, Bundle savedInstanceState) {
        assert mMediaLoader != null;

        if (mMediaLoader instanceof GoogleMapsLoader){
            ((GoogleMapsLoader) mMediaLoader).loadGoogleMaps(getActivity(), rootView, savedInstanceState);
        }

        mMediaLoader.loadMedia(this, backgroundImage, rootView, new MediaLoader.SuccessCallback() {
            @Override
            public void onSuccess() {
                createViewAttacher(getArguments());
            }
        });
    }

    private void createViewAttacher(Bundle savedInstanceState) {
        if (savedInstanceState.getBoolean(ZOOM)) {
            photoViewAttacher = new PhotoViewAttacher(backgroundImage);
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        outState.putSerializable(MEDIALOADER, mMediaLoader);
        outState.putBoolean(ZOOM, photoViewAttacher != null);
        super.onSaveInstanceState(outState);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case R.id.action_download:
                    CommonUtils.download(getActivity(),
                            ((MediaAttachment) mMediaLoader.getAttachment()).getUrl());
                return true;
            case R.id.action_navigate:
                double lat = ((LocationAttachment) mMediaLoader.getAttachment()).getLat();
                double lon = ((LocationAttachment) mMediaLoader.getAttachment()).getLon();

                Uri gmmIntentUri = Uri.parse("google.navigation:q="+lat+","+lon);
                Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                mapIntent.setPackage("com.google.android.apps.maps");
                if (mapIntent.resolveActivity(getActivity().getPackageManager()) != null) {
                    startActivity(mapIntent);
                }
                return true;
            case R.id.action_wallpaper:
                Picasso.with(getContext()).load(((MediaAttachment) mMediaLoader.getAttachment()).getUrl()).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        WallpaperManager wallpaperManager = WallpaperManager.getInstance(getContext());
                        try {
                            wallpaperManager.setBitmap(bitmap);

                            Toast toast = Toast.makeText(getContext(), getString(R.string.wallpaper_success), Toast.LENGTH_SHORT);
                            toast.show();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {
                    }
                });
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        if (mMediaLoader.getAttachment() instanceof MediaAttachment) {
            inflater.inflate(R.menu.menu_download, menu);

            //If this media is an image, also inflate the wallpaper button.
            if (mMediaLoader.getAttachment() instanceof MediaAttachment &&
                    ((MediaAttachment) mMediaLoader.getAttachment()).getMime().equals(MediaAttachment.MIME_IMAGE))
                inflater.inflate(R.menu.menu_image, menu);
        } else if (mMediaLoader.getAttachment() instanceof LocationAttachment) {
            inflater.inflate(R.menu.menu_maps, menu);
        }
        super.onCreateOptionsMenu(menu,inflater);
    }

    private boolean isViewPagerActive() {
        return viewPager != null;
    }

}
