package com.example.fresh.modulio.adapter;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.afrozaar.wp_api_v2_client_android.model.Comment;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.example.fresh.modulio.logic.CommonUtils;
import com.example.fresh.modulio.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */

public class CommentAdapter extends BaseQuickAdapter<Comment> {

    private Context context;

    public CommentAdapter(Context context, List<Comment> data) {
        super(R.layout.fragment_comments_comment, data);
        this.context = context;
    }

    @Override
    protected void convert(final BaseViewHolder baseViewHolder, final Comment comment) {
        baseViewHolder.setText(R.id.userView, comment.getAuthorName())
                .setText(R.id.contentView, CommonUtils.stripHtml(comment.getContent().getRendered()))
                .setText(R.id.dateView, CommonUtils.relativeDateFromWp(context, comment.getDate()))
                .addOnClickListener(R.id.replyButton);
        Picasso.with(mContext).load(comment.getAuthorAvatarUrl("96")).into((ImageView) baseViewHolder.getView(R.id.avatarView));

        //Show the comment depth
        LinearLayout lineView = baseViewHolder.getView(R.id.lineView);
        lineView.removeAllViews();

        for (int i = 0; i < getCommentLevel(comment); i++) {
            View line = View.inflate(context, R.layout.fragment_comments_comment_sub, null);
            lineView.addView(line);
        }
    }

    private int getCommentLevel(Comment comment) {
        //Base case
        if (comment.getParent() == 0) return 0;

        //If it has a parent, it has a level that is 1 deeper than the parents
        for (Comment parentCandidate : getData()){
            if (parentCandidate.getId() == comment.getParent()){
                return getCommentLevel(parentCandidate) + 1;
            }
        }

        //Default case
        return 0;

    }
}