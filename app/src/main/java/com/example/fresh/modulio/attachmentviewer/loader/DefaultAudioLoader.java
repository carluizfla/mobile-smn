package com.example.fresh.modulio.attachmentviewer.loader;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.View;
import android.widget.ImageView;

import com.example.fresh.modulio.attachmentviewer.model.MediaAttachment;
import com.example.fresh.modulio.attachmentviewer.ui.AttachmentFragment;
import com.example.fresh.modulio.attachmentviewer.ui.AudioPlayerActivity;
import com.example.fresh.modulio.R;

/**
 * This file is part of the Modulio template
 * For license information, please check the LICENSE
 * file in the root of this project
 *
 * @author Sherdle
 * Copyright 2016
 */
public class DefaultAudioLoader extends MediaLoader {

    public DefaultAudioLoader(MediaAttachment attachment) {
        super(attachment);
    }

    @Override
    public boolean isImage() {
        return false;
    }

    @Override
    public void loadMedia(final AttachmentFragment context, ImageView imageView, View rootView, SuccessCallback callback) {
        imageView.setImageBitmap(((BitmapDrawable) context.getResources().getDrawable(R.drawable.no_coverart)).getBitmap());

        View.OnClickListener playClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playAudio(context.getContext(), ((MediaAttachment) getAttachment()).getUrl(), getAttachment().getDescription());
            }
        };

        imageView.setOnClickListener(playClickListener);

        rootView.findViewById(R.id.playButton).setVisibility(View.VISIBLE);
        rootView.findViewById(R.id.playButton).setOnClickListener(playClickListener);
    }

    @Override
    public void loadThumbnail(Context context, ImageView thumbnailView, SuccessCallback callback) {
        thumbnailView.setImageBitmap(((BitmapDrawable) context.getResources().getDrawable(R.drawable.placeholder_audio)).getBitmap());
        callback.onSuccess();
    }

    private void playAudio(Context context, String url, String name) {
        AudioPlayerActivity.startActivity(context, url, name);
    }

}
