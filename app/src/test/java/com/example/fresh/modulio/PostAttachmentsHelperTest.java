package com.example.fresh.modulio;

/**
 * To work on unit tests, switch the Test Artifact in the Build Variants view.
 */
public class PostAttachmentsHelperTest {
    /**
    @Test
    public void videosImages() throws Exception {
        ArrayList<Attachment> attachments = new ArrayList<>();
        attachments.add(new MediaAttachment(null, MediaAttachment.MIME_PATTERN_VID, null));
        attachments.add(new MediaAttachment(null, MediaAttachment.MIME_IMAGE, null));
        attachments.add(new MediaAttachment(null, MediaAttachment.MIME_IMAGE, null));

        assertTrue("All are image or video",PostAttachmentsHelper.onlyImageOrVideoButOne(false, attachments) == null);

        Attachment other1 = new MediaAttachment(null, MediaAttachment.MIME_PATTERN_AUDIO, null);
        attachments.add(other1);
        assertTrue("All but 1 are image or video",PostAttachmentsHelper.onlyImageOrVideoButOne(false, attachments) == other1);

        attachments.add(new MediaAttachment(null, MediaAttachment.MIME_PATTERN_AUDIO, null));
        assertTrue("All but 2 are image or video",PostAttachmentsHelper.onlyImageOrVideoButOne(false, attachments) == null);
    }

    @Test
    public void imagesOnly() throws Exception {
        ArrayList<Attachment> attachments = new ArrayList<>();
        Attachment video = new MediaAttachment(null, MediaAttachment.MIME_PATTERN_VID, null);
        attachments.add(video);
        attachments.add(new MediaAttachment(null, MediaAttachment.MIME_IMAGE, null));
        attachments.add(new MediaAttachment(null, MediaAttachment.MIME_IMAGE, null));

        assertFalse("All are image",PostAttachmentsHelper.onlyImageOrVideoButOne(true, attachments) == null);

        assertTrue("All but 1 are image",PostAttachmentsHelper.onlyImageOrVideoButOne(true, attachments) == video);

        attachments.add(new MediaAttachment(null, MediaAttachment.MIME_PATTERN_AUDIO, null));
        assertTrue("All but 2 are image",PostAttachmentsHelper.onlyImageOrVideoButOne(true, attachments) == null);
    }**/

}